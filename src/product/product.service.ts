import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
let product: Product[] = [
  { id: 1, name: 'พัดลมฮาตาริ', price: 499 },
  { id: 2, name: 'หม้อหุงข้าว', price: 399 },
  { id: 3, name: 'เตารีดไฟฟ้า', price: 299 },
];
let lastUserId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastUserId++,
      ...createProductDto,
    };
    product.push(newProduct);
    return newProduct;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateUser: Product = {
      ...product[index],
      ...updateProductDto,
    };
    product[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = product.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteUser = product[index];
    product.splice(index, 1);
    return deleteUser;
  }
  reset() {
    product = [
      { id: 1, name: 'พัดลมฮาตาริ', price: 499 },
      { id: 2, name: 'หม้อหุงข้าว', price: 399 },
      { id: 3, name: 'เตารีดไฟฟ้า', price: 299 },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
